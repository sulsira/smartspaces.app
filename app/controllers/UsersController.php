<?php

class UsersController extends AdminController {

	/**
	 * Display a listing of the resource.
	 * GET /users
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = User::with('role')->get();
		$users = $users ? $users->toArray() : []; 

		// $this->layout->content = View::make('admin.Users.index')->with('users',$users);
		// $users = User::where('deleted','!=',1)->get()->toArray();
		// // $this->data['users'] = $users;
		// dd(($users));
		$this->layout->content = View::make('admin.Users.index', compact('users'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /users/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /users
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();

		$u  = new services\validators\User;
		if($u->passes()){
			$user = User::create(array(
				'email'=> $input['email'],
				'password'=>Hash::make($input['password']),
				'deleted'=> 0)
			);
			if ($user) {
				UserRole::create(array(
					'user_id' => $user->id,
					'fullname' => $input['username'],
					'privileges' => $input['Previleges'],
					'userGroup' => $input['accountType'],
					'security_level' => $input['security_level']
				));
				Flash::overlay('Your have added a user');
				return Redirect::back();
			}
		}else{
			$errors = $u->errors;
			return Redirect::back()->withErrors($errors)->withInput();							
		}
	}

	/**
	 * Display the specified resource.
	 * GET /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /users/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = User::with('role')->where('id','=',$id)->first();
		$user = (!empty($user))? $user->toArray() : [];
        $this->layout->content =  View::make('admin.Users.edit')->with('user',$user);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input  = Input::all();
		// dd($input);
	$rules = [
		'email'=>'email',
	];
		$validation = Validator::make($input,$rules);

		if ($validation->fails()) {
			return Redirect::back()->withErrors($validation)->withInput();
		}

		if(isset($input['user_id'])):
			$user = User::findOrFail($id);
			// $user->fill($input);
			$user->password = Hash::make($input['password']);
			$user->accessCode = $input['accessCode'];
			$user->save();
		endif;

		if(!empty($input['userRole_id']) && isset($input['userRole_id'])):
			$role = UserRole::findOrFail($input['userRole_id']);
			$role->fill($input);
			$role->save();
		endif;
		return Redirect::to('users')->withErrors($validation)->withInput();
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = User::with('role')->where('id','=',$id)->first();

		$role = (!empty($user->role))? $user->toArray() : [];
		// $role = (!empty($user->role))? UserRole::destroy($user->role()->id) : [];
		if(!empty($role)):
			UserRole::destroy($role['id']);
		endif;
		
		$user = User::destroy($id);
		return Redirect::to('users');
	}

}