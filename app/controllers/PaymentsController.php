<?php

class PaymentsController extends \AdminController {

	/**
	 * Display a listing of the resource.
	 * GET /payments
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /payments/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('admin.payments.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /payments
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$paid = array();
		$pay  = new services\Validators\Landpayment;

		if($pay->passes()){

 		$cus = Plot::with('customer')->where('plot_id','=',$input['payment_plot'])->first();
		// $cus = Customer::with(['plots'=>function($query){
		// 	$query->where('plot_id','=',$inputs['payment_plot']);
		// }])->where('cust_id','=',$input['customer_id'])->get();
		$customer = (!empty($cus)) ? $cus->toArray() : []; #secure

		if(!empty($customer)):

			if ( isset($input['amount_paid']) && is_numeric($input['amount_paid']) ):
					$paid = Payment::create(array(
						'paym_userID' =>Session::get('user_id'),
						'paym_custID' => $input['customer_id'],
						'paym_paidAmount' => $input['amount_paid'],
						'paym_plotID' => $input['payment_plot']
					));

				if(!empty($paid)):

					if ( $customer['customer']['total_payment'] == '0.00'):

						$cust = Customer::find($input['customer_id']);
						$cust->total_payment = $customer['customer']['cust_downpayment'] + $input['amount_paid'] ;
						$cust->Balance = $customer['plot_price']  - ( $customer['customer']['cust_downpayment'] + $input['amount_paid'] ) ;
						$cust->save();

					else:

						$cust = Customer::find($input['customer_id']);
						$cust->total_payment = $customer['customer']['total_payment'] + $input['amount_paid'] ;
						$cust->save();

						$cust2 = Customer::find($input['customer_id']);
						$cust2->Balance = $customer['plot_price']  - $cust2->total_payment ;
						$cust2->save();

					endif;

					// $cust->total_payment = ($cus->cust_downpayment + $input['amount_paid']) - ; #balace payment - plot price


				endif;
			endif;

		endif;
		// $customer = $cus ? $cus->toArray() : []; #secure

		// $this->data  = $customer ;


			// Record the payment in payment table

					// dd($paid->toArray());
					// $trans = Transaction::create(array(
					// 	'trans_custID' => ,
					// 	'trans_payID' => ,
					// 	'trans_currentBal' => ,
					// 	'trans_dueBal' => ,
					// 	'trans_totalBal' => ,
					// 	'trans_status' => ,
					// 	'trans_visible' => 
					// 	));

				Flash::overlay('The payment had been made');
				return Redirect::back();
			// replicate and change the customer transactions

			// display the balance of the cutomer


			// $user = User::create(array(
			// 	'email'=> $input['email'],
			// 	'hpwd'=>Hash::make($input['password']),
			// 	'deleted'=> 1)
			// );
			// if ($user) {
			// 	UserRole::create(array(
			// 		'user_id' => $user->id,
			// 		'fullname' => $input['username'],
			// 		'privileges' => $input['Previleges'],
			// 		'userGroup' => $input['accountType']
			// 	));
			// 	Flash::overlay('Your have added a user');
			// 	return Redirect::back();
			// }


		}else{
			$errors = $pay->errors;
			return Redirect::back()->withErrors($errors)->withInput();							
		}
	}

	/**
	 * Display the specified resource.
	 * GET /payments/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$this->layout->content = View::make('admin.payments.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /payments/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /payments/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /payments/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}