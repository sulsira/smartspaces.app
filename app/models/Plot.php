<?php

class Plot extends \Eloquent {
	protected $primaryKey = 'plot_id';
	protected $fillable = [
		'plot_id',
		'plot_estateID',
		'plot_name',
		'plot_size',
		'plot_price',
		'plot_location',
		'plot_lon',
		'plot_lat',
		'plot_number',
		'plot_status', #0 - means no-owner, 1 = has an owner , 2 = means transfered
		'plot_remarks',
		'plot_availability', # null - no actions, available = for sale, not-available = not for sale; 
		'plot_cusID',
		'plot_agenID',
		'total_payment',
		'period',
		'Balance',
		'Monthly_Fee',
		'Commencing'
	];

	public function customer(){
		return $this->belongsTo('Customer','plot_id','cust_plotID');
	}
	public function payments(){
		return $this->hasMany('Payment','paym_plotID','plot_id');
	}
	public function estate(){
		return $this->belongsTo('Estate','plot_estateID','est_id');
	}
	public function agent(){
		return $this->belongsTo('Agent','plot_agenID');
	}
	public function scopeAvailable($query){
		return $query->whereRaw('plot_availability = ? OR plot_availability = ?',['available',NULL])->get();
	}
	public function scopeUnassigned($query){
		return $query->whereRaw('plot_agenID = ? AND plot_status = ? ',[0, 0])->get();
	}
}


