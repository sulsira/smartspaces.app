<?php

class Agent extends \Eloquent {
	protected $primaryKey = 'agen_id';
	protected $fillable = ['agen_persID','agen_contID','agen_addrID','agen_prospID'];

	public function person(){
		return $this->belongsTo('Person','agen_persID','id');
	}
	public function plots(){
		return $this->hasMany('Plot','plot_agenID');
	}
	public function compounds(){
		return $this->hasMany('Compound','agendid');
	}
}