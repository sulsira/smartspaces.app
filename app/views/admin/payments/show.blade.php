@include('templates/top-admin')
@section('content')
	<div class="c-header cc">
		<h3>Plots</h3>
	</div>
	<div class="cc">
		<table class="table">
			<thead>
				<tr>
					<th>#</th>
					<th>Plot Name</th>
					<th>Plot Size</th>
					<th>Plot Price</th>
					<th>Plot Created</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php if (!empty($Plots)): ?>
				<?php $depart = $Plots->get()->toArray(); ?>
					<?php foreach ( $depart as $key => $value ): ?>
						<tr>
							<td>{{$key + 1}}</td>
							<td>{{e($value['name'])}}</td>
							<td>{{e($value['personName'])}}</td>
							<td>
								<?php if (!empty($value['courses'])): ?>
									<ul class="courseLi">
										<?php foreach ($value['courses'] as $key2 => $value2): ?>
											<li><a href="{{route('Plots.courses.show',[$value['id'],$value2['id']])}}">{{$value2['name']}}</a></li>	
										<?php endforeach ?>
									</ul>									
								<?php endif ?>
							</td>
							<td>{{e($value['created_at'])}}</td>
							<td>{{e($value['updated_at'])}}</td>
						</tr>
					<?php endforeach ?>
					<?php else: ?>
					<tr>
						<td colspan="6"><h4>No Plot!</h4></td>
					</tr>
				<?php endif ?>
			</tbody>
		</table>
	</div>
@stop
@include('templates/bottom-admin')