@include('templates/top-admin')
@section('content')
	<div class="cc">
		<div class="icons_zone">
			<p>
				<a href="notifications" class="imgthumb">
					<img src="__public/img/notifications.png">
				</a>

				<!-- <a href="notifications">Notifications</a> -->
			</p>
			<p>
				<a href="{{route('prospectives.index')}}" class="imgthumb">
					<!-- <i class="fa fa-tags"></i>  -->
					<img src="__public/img/customers.png">

				</a>
				<!-- <a href="{{route('prospectives.index')}}">Prospective Customers</a> -->
			</p>
			<p>
				<a href="{{route('agents.index')}}" class="imgthumb">
					<!-- <i class="fa fa-users"></i>  -->
					<img src="__public/img/agents.png">

				</a>
				<!-- <a href="{{route('agents.index')}}">Agents</a> -->
			</p>
			<p>
				<a href="{{route('plots.index')}}" class="imgthumb">
					<!-- <i class="fa fa-map-marker"></i> -->
					<img src="__public/img/plots.png">

				</a>
				<!-- <a href="{{route('plots.index')}}">Plots</a> -->
			</p>
			<p>
				<a href="{{route('houses.index')}}" class="imgthumb">
					<!-- <i class="fa fa-building-o"></i> -->
					<img src="__public/img/house.png">

				</a>
				<!-- <a href="{{route('houses.index')}}">Houses</a> -->
			</p>
			<p>
				<a href="{{route('tenants.index')}}" class="imgthumb">
					<!-- <i class="fa fa-key"></i> -->
					<img src="__public/img/tenants.png">

				</a>
				<!-- <a href="{{route('tenants.index')}}">Tenant</a> -->
			</p>
			<p>
				<a href="{{route('data_entry')}}" class="imgthumb">
					<!-- <i class="fa fa-pencil-square-o"></i> -->
					<img src="__public/img/dataentery.png">

				</a>
				<!-- <a href="{{route('data_entry')}}">Data Enty</a> -->
			</p>
			<p>
				<a href="{{route('users.index')}}" class="imgthumb">
					<!-- <i class="fa fa-user"></i> -->
					<img src="__public/img/users.png">

				</a>
				<!-- <a href="{{route('users.index')}}">Users</a> -->
			</p>
		</div><!-- content conent -->	
	</div>
@stop
@include('templates/bottom-admin')