@include('templates/top-admin')
@section('content')
@include('__partials/modal-add-house')
	<div class="c-header cc">
		<h3>Houses <!-- <a href="#myModal" role="button" data-toggle="modal"><i class="fa fa-plus"></i></a> --></h3>
	</div>
	<div class="cc">
				<div class="messages">
					@include('flash::message')
					@include('__partials/errors')
				</div>
				<table class="table">
					<thead>
						<tr>
							<th>House</th>
							<th>Compound</th>
							<th>Price</th>
							<th>Description</th>
							<th>Available</th>
							<th>created</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<?php if (!empty($houses)): ?>
							<?php foreach ($houses as $key => $value): ?>
							<tr>
							<td><a href="{{route('houses.show',$value['hous_id'])}}">{{$value['hous_number']}}</a></td>
							<td><a href="{{route('compounds.show',$value['compound']['comp_id'])}}">{{$value['compound']['comp_indentifier']}}</a></td>
								<td>{{$value['hous_price']}}</td>
								<td>{{$value['hous_description']}}</td>
								<td><?php echo ($value['hous_availability']) ?: 'occupied' ?></td>
								<td>{{$value['created_at']}}</td>
								<td>
									<a href="{{route('houses.show',$value['hous_id'])}}">view </a>| 
									<a href="{{route('houses.destroy',$value['hous_id'])}}" class="dropdown-toggle"  data-toggle="dropdown">
										Options
										<!-- <span class="caret"></span> -->
									</a>
									  <ul class="dropdown-menu">
									    <!-- dropdown menu links -->
									    <li>
									    	{{Form::delete('houses/'. $value['hous_id'], 'Delete')}}
									    </li>
									  </ul>
								</td>
							</tr>						
							<?php endforeach ?>
							<?php else: ?>
							<tr>
								<td colspan="10"><h4>No Houses Available!</h4></td>
							</tr>
						<?php endif ?>
					</tbody>
				</table>

		  </div>
		</div>
	</div>
@stop
@include('templates/bottom-admin')